# Autor: Zaqueu Ricardo
# Data: 26-03-2021

# Comment

Write-Output "Estudo de variaveis"

$texto = "Qualquer string"
Write-Output $texto # Mostra o valor que esta guardado na variável

$numero = 10.0 # Valores com casas decimal
$idade = 28

Write-Output "Numero: $numero"
Write-Output "Idade: $idade"