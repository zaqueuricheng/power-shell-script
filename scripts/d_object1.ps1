# Autor: Zaqueu Ricardo
# Data: 26-03-2021

# Uso da class WScript.Shell
# Essa class contém uma coleção de métodos úteis para a criação de scripts no Windows

# Class WScript.Shell
$wshell = New-Object -com WScript.Shell # Cria um objeto para acessar tudo que tem dentro da class

# Lista de comandos
$wshell | Get-Member

# Caixa de mensagem
$wshell.Popup("O PowerShell é muito loco mesmo")

# Executando a calculadora
$wshell.Run("Calc") # Run, para abrir qualquer aplicativo do windows