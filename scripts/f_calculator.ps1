# Autor: Zaqueu Ricardo
# Data: 26-03-2021

# Calculadora basica

[int]$n1 = Read-Host "Digite n1: " # use [int], [float] para converter o dado de entrada, caso n usar ele concatena
$n2 = Read-Host "Digite n2: "

$soma = $n1+$n2
$subtracao= $n1-$n2
$multiplicacao= $n1*$n2
$divisao= $n1/$n2

Write-Output "($n1 + $n2) = $soma" 
Write-Output "($n1 - $n2) = $subtracao" 
Write-Output "($n1 x $n2) = $multiplicacao"
Write-Output "($n1 / $n2) = $divisao"