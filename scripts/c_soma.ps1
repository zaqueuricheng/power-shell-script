# Autor: Zaqueu Ricardo
# Data: 26-03-2021

$n1 = 10
$n2 = 2+3
$soma = $n1+$n2

Write-Output "Soma = ($n1 + $n2)" # Não faz a soma, mostra somente os valores das variaveis n1 e n2
Write-Output "Soma = $soma" # Mostra a soma das variaveis n1 e n2 feitas pela variavel soma