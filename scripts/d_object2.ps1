# Autor: Zaqueu Ricardo
# Data: 26-03-2021

# Uso da class WScript.Shell
# Essa class contém uma coleção de métodos úteis para a criação de scripts no Windows

# Class WScript.Shell
$wshell = New-Object -com WScript.Shell # Cria um objeto para acessar tudo que tem dentro da class

# Executando o bloco de notas
$wshell.Run("Notepad") # Run, para abrir qualquer aplicativo do windows

# Ativando o notpad
$wshell.AppActivate("Notepad")

# Espera de 1s
Start-Sleep 1

$wshell.Sendkeys("Interessante!!!") # Envia o conteudo dentro do bloco de notas