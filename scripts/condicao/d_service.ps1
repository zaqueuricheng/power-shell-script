# Autor: Zaqueu Ricardo
# Data: 27-03-21

# Script q verifica se algum serviço esta instalado

# Limpa tela
clear-host

# Nome do serviço
$svcname = "Audio do Windows "

# Captura se o serviço esta instalado
$service = get-service -displayName $svcname -ErrorAction silentlyContinue

# Verifica se a variavel não tem retorno
if(-not $svcname){
    $svcname + "Não esta instalado nesse computador"
}else{
    $svcname + "esta instalado. "+$svcname+ "o status eh:" + $service.status
}